<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Главная</title>
    
    <link rel="stylesheet" href="/public/css/style.css">
    <link rel="stylesheet" href="/public/css/bootstrap.min.css">
    <link rel="stylesheet" href="/public/font-awesome/css/font-awesome.min.css">
</head>

<style>
    a { 
        color: white;
        text-decoration: none;
    }
</style>

<body>
    <?php include 'modules/header.php'; ?>
    <?php include 'app/views/'.$content_view; ?>

    <script src="/public/js/bootstrap.bundle.min.js"></script>
</body>
</html>