<div class="p-3">
    <div type="button" class="btn btn-success" data-toggle="modal" data-target="#taskModal">Добавить задачу</div>
    <div class="modal fade" id="taskModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Добавить задачу</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="/add/">
                        <input type="text" name="name" class="form-control" placeholder="Имя" required>
                        <input type="email" name="email" class="form-control" placeholder="Почта" required>
                        <input type="text" name="task" class="form-control" placeholder="Задача" required>
                        <hr>
                        <button class="btn btn-success btn-round" type="submit">Добавить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if(!empty($data['error'])): ?>
  <div class="alert alert-danger" role="alert"><?php echo ($data['error']);?></div>
<?php elseif(!empty($data['notification'])): ?>
  <div class="alert alert-success" role="alert"><?php echo ($data['notification']);?></div>
<?php endif; ?>

<table class="table table-dark table-borderless">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col"><a href="/?page=<?php echo !isset($_GET['page']) ? 1 : $_GET['page']; ?>&sort=<?php echo !isset($_GET['sort']) || $_GET['sort'] != '1' ? 1 : 2; ?>">Имя <i class="fa fa-sort" aria-hidden="true"></i></a></th>
            <th scope="col"><a href="/?page=<?php echo !isset($_GET['page']) ? 1 : $_GET['page']; ?>&sort=<?php echo !isset($_GET['sort']) || $_GET['sort'] != '3' ? 3 : 4; ?>">Почта <i class="fa fa-sort" aria-hidden="true"></i></a></th>
            <th scope="col">Задача</th>
            <th class="sorting" scope="col"><a href="/?page=<?php echo !isset($_GET['page']) ? 1 : $_GET['page']; ?>&sort=<?php echo !isset($_GET['sort']) || $_GET['sort'] != '5' ? 5 : 6; ?>">Статус <i class="fa fa-sort" aria-hidden="true"></i></a></th>
            <?php if($_SESSION['auth'] == true): ?>
            <th scope="col">Действия</th>
            <?php else:?>
            <?php endif; ?>
        </tr>
    </thead>
    <tbody>
      <?php foreach($data['tasks'] as $row): ?>
        <tr>
            <th scope="row"><?php echo $row['id']; ?></th>
            <th scope="row"><?php echo htmlspecialchars($row['name']); ?></th>
            <th scope="row"><?php echo htmlspecialchars($row['email']); ?></th>
            <th scope="row"><?php echo htmlspecialchars($row['text']); ?></th>
            <?php if($row['status'] == 0 and $row['edit'] == 0): ?>
              <th scope="row"><i class="fa fa-times" aria-hidden="true" style="color: red;"></i></th>
            <?php elseif(($row['status'] == 0 and $row['edit'] == 1)):?>
              <th scope="row"><i class="fa fa-times" aria-hidden="true" style="color: red;"></i><p class="editing">Изменено администратором</p></th>
            <?php elseif(($row['status'] == 1 and $row['edit'] == 1)):?>
              <th scope="row"><i class="fa fa-check" aria-hidden="true" style="color: green;"></i><p class="editing">Изменено администратором</p></th>
            <?php else:?>
              <th scope="row"><i class="fa fa-check" aria-hidden="true" style="color: green;"></i></th>
            <?php endif; ?>
          
            <?php if($_SESSION['auth'] == true): ?>
              
              <th scope="row">
                <?php if($row['status'] == 0): ?>
                  <button type="button" class="btn btn-success"><a href="/complete/?id=<?php echo $row['id']; ?>">Завершить</a></button>
                <?php else:?>
                <?php endif; ?>
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#taskEditModal-<?php echo $row['id']; ?>">Редактировать</button>
              </th>
              <div class="modal fade" id="taskEditModal-<?php echo $row['id']; ?>" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <h5 class="modal-title">Редактировать задачу</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                      </div>
                      <div class="modal-body">
                          <form method="POST" action="/editing/" id="task=<?php echo $row['id']; ?>">
                              <input class="form-check-input" type="text" name="id" value="<?php echo $row['id']; ?>" hidden="true">
                              <input type="text" name="editTask" class="form-control" value="<?php echo $row['text']; ?>" required>
                              <hr>
                              <button class="btn btn-success btn-round" type="submit">Редактировать</button>
                          </form>
                      </div>
                  </div>
              </div>
          </div>
            <?php else:?>              
            <?php endif; ?>
        </tr>
      <?php endforeach; ?>
    </tbody>
</table>


<nav aria-label="Page navigation example">
  <ul class="pagination justify-content-center">
    <?php 
        for($i=1; $i<= $data['countPages']; $i++) {
            if(!isset($_GET['sort'])) echo "<li class=\"page-item\"><a class=\"page-link\" href=\"\?page={$i}\" style=\"color: black;\">{$i}</a></li>";
            else echo "<li class=\"page-item\"><a class=\"page-link\" href=\"\?page={$i}&sort={$_GET['sort']}\" style=\"color: black;\">{$i}</a></li>";
        }
    ?>
  </ul>
</nav>
