<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="/">BeeJee</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto mb-2 mb-lg-0">
            </ul>
            <?php if(!$_SESSION['auth']):?>
            <div type="button" class="btn btn-success" data-toggle="modal" data-target="#authModal">Вход</div>
            <div class="modal fade" id="authModal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Вход на сайт</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="/signin/">
                                <input type="text" name="login" class="form-control" placeholder="Логин" required>
                                <input type="password" name="password" class="form-control" placeholder="Пароль"
                                    required>
                                <hr>
                                <button class="btn btn-success btn-round" type="submit">Войти</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <?php else:?>
            <a type="button" class="btn btn-success" href="/logout/">Выйти</a>
            <?php endif;?>
        </div>
</nav>