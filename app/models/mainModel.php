<?php

namespace App\models;

use App\core\model;

class mainModel extends model {
    public function auth() {
        $login = $_POST['login'];
        $password = $_POST['password'];
        $sql = "SELECT * FROM admins WHERE login = :login AND password = :password";

        $state = $this->db->prepare($sql);
        $state->bindParam(":login", $login);
        $state->bindParam(":password", $password);
        $state->execute();

        if(!empty($state->fetch(\PDO::FETCH_ASSOC))){
            $_SESSION['auth'] = true;
        }else {
            $errors = 'Неверное имя пользователя или пароль';
            return $errors;
        }
    }

    public function addTask() {
        $name = $_POST['name'];
        $email = $_POST['email'];
        $text = $_POST['task'];

        $sql = 'INSERT INTO tasks (name,email,text) VALUES (:name,:email,:text);';

        $state = $this->db->prepare($sql);
        $state->bindParam(":name", $name);
        $state->bindParam(":email", $email);
        $state->bindParam(":text", $text);
        $state->execute();

        $notification = 'Задача успешно добавлена';
        return $notification;
    }

    public function getCountPages() {
        $limit = 3;
        $sql  = 'SELECT COUNT(*) FROM tasks';
        $state = $this->db->prepare($sql);
        $state->execute();
        $tasks = $state->fetch();

        $total_page = ceil($tasks['0'] / $limit);

        return $total_page;
    }

    public function getTaskOnPages() {
        $total_page = $this->getCountPages();

        if(!isset($_GET['page']) || intval($_GET['page']) == 0){
            $page = 1;
        }else if (intval($_GET['page']) > $total_page) {
            $page = $total_page;
        }else $page = intval($_GET['page']);

        $limit = 3;
        $offset = ($page - 1) * $limit;
        if (!isset($_GET['sort'])) $sql  = "SELECT * FROM tasks LIMIT $offset, $limit";
        elseif ($_GET['sort'] == '1') $sql  = "SELECT * FROM tasks order by name asc LIMIT $offset, $limit";
        elseif ($_GET['sort'] == '2') $sql  = "SELECT * FROM tasks order by name desc LIMIT $offset, $limit";
        elseif ($_GET['sort'] == '3') $sql  = "SELECT * FROM tasks order by email asc LIMIT $offset, $limit";
        elseif ($_GET['sort'] == '4') $sql  = "SELECT * FROM tasks order by email desc LIMIT $offset, $limit";
        elseif ($_GET['sort'] == '5') $sql  = "SELECT * FROM tasks order by status asc LIMIT $offset, $limit";
        elseif ($_GET['sort'] == '6') $sql  = "SELECT * FROM tasks order by status desc LIMIT $offset, $limit";

        $state = $this->db->prepare($sql);
        $state->execute();
        
        return $state->fetchAll();
    }
}