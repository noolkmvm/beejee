<?php

namespace App\models;

use App\core\model;

class adminModel extends model{
    public function updateTask() {
        $id = $_POST['id'];
        $text = $_POST['editTask'];
        $edit = 1;
        
        $sql = "UPDATE tasks SET text = ?, edit = ? WHERE id = ?";

        $state = $this->db->prepare($sql);
        $state->bindParam(1, $text);
        $state->bindParam(2, $edit);
        $state->bindParam(3, $id);
        $state->execute();

        header("Location: /");
    }

    public function updateStatus() {
        $id = $_GET['id'];
        $status = 1;
        
        $sql = "UPDATE tasks SET status = ? WHERE id = ?";

        $state = $this->db->prepare($sql);
        $state->bindParam(1, $status);
        $state->bindParam(2, $id);
        $state->execute();

        header("Location: /");
    }
}