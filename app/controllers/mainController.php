<?php

namespace App\controllers;

use App\core\controller;
use App\core\view;
use App\models\mainModel;

class mainController extends controller {
	function __construct(){
		$this->model = new mainModel();
		$this->view = new view();
	}

	function action_index(){
		$data['tasks'] = $this->model->getTaskOnPages();
		$data['countPages'] = $this->model->getCountPages();

		$this->view->generate('mainView.php', 'template.php', $data);
	}

	public function action_login(){
		$data['error'] = $this->model->auth();
		$data['tasks'] = $this->model->getTaskOnPages();
		$data['countPages'] = $this->model->getCountPages();
		$this->view->generate('mainView.php', 'template.php', $data);
	}
	
	public function action_logout(){
		session_destroy();
		header("Location: /");
	}

	public function action_addTask(){
		$data['notification'] = $this->model->addTask();
		$data['tasks'] = $this->model->getTaskOnPages();
		$data['countPages'] = $this->model->getCountPages();
		$this->view->generate('mainView.php', 'template.php', $data);
	}

	public function action_getCountTasks(){
		$this->model->getTaskOnPages();
	}
}