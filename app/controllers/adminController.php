<?php

namespace App\controllers;

use App\core\controller;
use App\models\adminModel;
use App\core\view;

class adminController extends controller{
	function __construct(){
		$this->model = new adminModel();
		$this->view = new view();
	}

	public function action_updateTask(){
		$this->model->updateTask();
	}

	public function action_updateStatus(){
		$this->model->updateStatus();
	}
}