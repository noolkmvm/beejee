<?php

namespace app\config;

class database{
	const USER = "root";
	const PASS = 'root';
	const HOST = "localhost";
	const DB   = "beejee";

	public static function connectDatabase() {
		$user = self::USER;
		$pass = self::PASS;
		$host = self::HOST;
		$db   = self::DB;

		$connect = new \PDO("mysql:dbname=$db;host=$host", $user, $pass);
		return $connect;
    }
}