<?php

namespace App\core;
use App\core\view;

class controller {
	
	public $model;
	public $view;
	
	function __construct(){
		$this->view = new view();
	}
}