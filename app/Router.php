<?php

namespace App;

use App\controllers\mainController;
use App\controllers\adminController;

class Router{
    public static $routes = [
        '~^$~' => [mainController::class, 'action_index'],
        '~^add/$~' => [mainController::class, 'action_addTask'],
        '~^signin/~' => [mainController::class, 'action_login'],
        '~^logout/~' => [mainController::class, 'action_logout'],
        '~^editing/~' => [adminController::class, 'action_updateTask'],
        '~^complete/~' => [adminController::class, 'action_updateStatus'],
    ];

}
?>